import {pokemonMachine} from "~/machines/pokemon/machine";

export default function () {
  return useMachine(pokemonMachine);
};
