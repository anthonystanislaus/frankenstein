import {toggleMachine} from "~/machines/toggle/machine";

export default function () {
  return useMachine(toggleMachine);
};
