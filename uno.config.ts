import {defineConfig, presetAttributify, presetIcons, presetUno, presetWebFonts} from "unocss";

export default defineConfig({
  rules: [
    // ["custom-rule", { color: "red" }],
  ],
  shortcuts: {
    // "custom-shortcut": "text-lg text-orange hover:text-teal",
  },
  presets: [
    presetUno(),
    presetAttributify(),
    presetWebFonts({
      provider: "fontshare",
      fonts: {
        sans: ["Satoshi"],
        mono: ["Boska",],
      },
    }),
    presetIcons({ scale: 1.2, cdn: "https://esm.sh/" }),
  ],
})
