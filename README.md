# At Play

### About

This is one of those repos where I intend throw a bunch of stuff together.
Largely concerning interesting/new tools, domain patterns and design ideas.

### Thoughts

Currently building with a state machine pattern (xstate).
This feels kinda MVC-like with state machines controlling/routing all logic
while all views will for the most part be uncontrolled.

Adding Effect to handle logic computations. This allows one to place some
guarantees on output and explicitly track potential error cases.
Gives me an [Elm](https://elm-lang.org)-like feel.

### Next up

- nested state machines
- local-first (maybe [Evolu](https://www.evolu.dev) or [PowerSync](https://docs.powersync.com))
- Graph-relational DBs ([EdgeDB](https://docs.edgedb.com), [SurrealDB](https://surrealdb.com/docs/surrealdb))
  - probably won't be able to sync with local sql-lite (though, edgedb is pg and surrealdb has an embed package...)

### Packages of Note

- [Effect](https://effect.website/docs): functional TypeScript w/Errors as first class
- [xState](https://stately.ai/docs): logic layer(state machine) enabling deterministic flows
