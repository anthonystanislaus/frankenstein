import {Schema} from "@effect/schema";
import {Context, Effect, Layer, Match, RequestResolver} from "effect";
import {Pokedex} from "pokeapi-js-wrapper";
import type {ApiRequest, ProgramResult} from "~/machines/pokemon/types";
import {
  GetPokemonCatalogError,
  GetPokemonError,
  Pokemon,
  PokemonByIdRequest,
  PokemonCatalog,
  PokemonCatalogRequest
} from "~/machines/pokemon/types";

// ************************************************************************************
// LAYER: API
// ************************************************************************************

const makePokemonApi = Effect.sync(() => {
  const pokedex = new Pokedex();
  return {
    getPokemonList: (): Effect.Effect<PokemonCatalog, GetPokemonCatalogError> =>
      Effect.tryPromise({
        try: () => pokedex.getPokemonsList().then((data) => data as PokemonCatalog),
        catch: () => new GetPokemonCatalogError()
      }),
    getPokemonById: (id: string | number): Effect.Effect<Pokemon, GetPokemonError> =>
      Effect.tryPromise({
        try: () => pokedex.getPokemonByName(id).then((d: any) => d as Pokemon),
        catch: () => new GetPokemonError()
      }),
  } as const;
});

class PokemonApi extends Context.Tag("PokemonApi")<
  PokemonApi,
  Effect.Effect.Success<typeof makePokemonApi>
>() { static readonly Live = Layer.effect(this, makePokemonApi) }

// ************************************************************************************
// LAYER: REPOSITORY
// ************************************************************************************

const makePokemonRepo = Effect.gen(function*(_) {
  const pokemonApi = yield* _(PokemonApi);

  const GetPokemonCatalogResolver = RequestResolver.fromEffect(
    (_request: PokemonCatalogRequest) =>
      pokemonApi.getPokemonList().pipe(
        Effect.andThen(Schema.decodeUnknownSync(PokemonCatalog)),
        Effect.andThen(Effect.succeed),
        Effect.catchAll(Effect.fail),
      )
  );

  const GetPokemonByIdResolver = RequestResolver.fromEffect(
    (request: PokemonByIdRequest) =>
      pokemonApi.getPokemonById(request.id).pipe(
        Effect.andThen(Schema.decodeUnknownSync(Pokemon)),
        Effect.andThen(Effect.succeed),
        Effect.catchAll(Effect.fail),
      )
  );

  return {
    getPokemonCatalog: () => Effect.request(new PokemonCatalogRequest(), GetPokemonCatalogResolver),
    getById: (id: string | number) => Effect.request(new PokemonByIdRequest({ id }), GetPokemonByIdResolver),
  } as const;
});

export class PokemonRepo extends Context.Tag("PokemonRepo")<
  PokemonRepo,
  Effect.Effect.Success<typeof makePokemonRepo>
>() { static readonly Live = Layer.effect(this, makePokemonRepo) }

// ************************************************************************************
// MAIN
// ************************************************************************************

const main = PokemonRepo.Live.pipe(
  Layer.provide(PokemonApi.Live),
);

export const PokemonLive = (request: ApiRequest): ProgramResult =>
  Effect.gen(function* (_) {
    const repo = yield* _(PokemonRepo);
    const router = Match.type<ApiRequest>().pipe(
      Match.tag("PokemonCatalogRequest", (_) => repo.getPokemonCatalog()),
      Match.tag("PokemonByIdRequest", (_) => repo.getById(_.id)),
      Match.exhaustive
    );
    const effect = yield* _(router(request));
    return yield* _(Effect.succeed(effect));
  }).pipe(
    Effect.provide(main),
  );
