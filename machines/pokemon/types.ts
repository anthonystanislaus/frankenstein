import {Schema} from "@effect/schema";
import {Data, Effect, Request} from "effect";

// ************************************************************************************
// SCHEMAS
// ************************************************************************************

export class PokemonCatalog extends Schema.Class<PokemonCatalog>("PokemonCatalog")({
  count: Schema.Int,
  next: Schema.NullOr(Schema.String),
  previous: Schema.NullOr(Schema.String),
  results: Schema.Array(Schema.Struct({name: Schema.String, url: Schema.String})),
}) {
}

export class Pokemon extends Schema.Class<Pokemon>("Pokemon")({
  id: Schema.Int,
  name: Schema.String,
  base_experience: Schema.Int,
  height: Schema.Int,
  is_default: Schema.Boolean,
  order: Schema.Int,
  weight: Schema.Int,
  abilities: Schema.Array(Schema.Struct({
    is_hidden: Schema.Boolean,
    slot: Schema.Int,
    ability: Schema.Struct({name: Schema.String, url: Schema.String}),
  })),
  forms: Schema.Array(Schema.Struct({name: Schema.String, url: Schema.String})),
  game_indices: Schema.Array(Schema.Struct({
    game_index: Schema.Int,
    version: Schema.Struct({name: Schema.String, url: Schema.String}),
  })),
  held_items: Schema.Array(Schema.Struct({
    item: Schema.Struct({name: Schema.String, url: Schema.String}),
    version_details: Schema.Array(Schema.Struct({
      rarity: Schema.Int,
      version: Schema.Struct({name: Schema.String, url: Schema.String}),
    })),
  })),
  location_area_encounters: Schema.String,
  moves: Schema.Array(Schema.Struct({
    move: Schema.Struct({name: Schema.String, url: Schema.String}),
    version_group_details: Schema.Array(Schema.Struct({
      level_learned_at: Schema.Int,
      version_group: Schema.Struct({name: Schema.String, url: Schema.String}),
      move_learn_method: Schema.Struct({name: Schema.String, url: Schema.String,}),
    })),
  })),
  species: Schema.Struct({name: Schema.String, url: Schema.String}),
  sprites: Schema.Struct({
    back_default: Schema.NullOr(Schema.String),
    back_female: Schema.NullOr(Schema.String),
    back_shiny: Schema.NullOr(Schema.String),
    back_shiny_female: Schema.NullOr(Schema.String),
    front_default: Schema.NullOr(Schema.String),
    front_female: Schema.NullOr(Schema.String),
    front_shiny: Schema.NullOr(Schema.String),
    front_shiny_female: Schema.NullOr(Schema.String),
    other: Schema.Struct({
      dream_world: Schema.Struct({
        front_default: Schema.NullOr(Schema.String),
        front_female: Schema.NullOr(Schema.String)
      }),
      home: Schema.Struct({
        front_default: Schema.NullOr(Schema.String),
        front_female: Schema.NullOr(Schema.String),
        front_shiny: Schema.NullOr(Schema.String),
        front_shiny_female: Schema.NullOr(Schema.String),
      }),
      "official-artwork": Schema.Struct({
        front_default: Schema.NullOr(Schema.String),
        front_shiny: Schema.NullOr(Schema.String)
      }),
      showdown: Schema.Struct({
        back_default: Schema.NullOr(Schema.String),
        back_female: Schema.NullOr(Schema.String),
        back_shiny: Schema.NullOr(Schema.String),
        back_shiny_female: Schema.NullOr(Schema.String),
        front_default: Schema.NullOr(Schema.String),
        front_female: Schema.NullOr(Schema.String),
        front_shiny: Schema.NullOr(Schema.String),
        front_shiny_female: Schema.NullOr(Schema.String),
      }),
    }),
    versions: Schema.Struct({
      "generation-i": Schema.Struct({
        "red-blue": Schema.Struct({
          back_default: Schema.NullOr(Schema.String),
          back_gray: Schema.NullOr(Schema.String),
          front_default: Schema.NullOr(Schema.String),
          front_gray: Schema.NullOr(Schema.String),
        }),
        yellow: Schema.Struct({
          back_default: Schema.NullOr(Schema.String),
          back_gray: Schema.NullOr(Schema.String),
          front_default: Schema.NullOr(Schema.String),
          front_gray: Schema.NullOr(Schema.String),
        }),
      }),
      "generation-ii": Schema.Struct({
        crystal: Schema.Struct({
          back_default: Schema.NullOr(Schema.String),
          back_shiny: Schema.NullOr(Schema.String),
          front_default: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String),
        }),
        gold: Schema.Struct({
          back_default: Schema.NullOr(Schema.String),
          back_shiny: Schema.NullOr(Schema.String),
          front_default: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String),
        }),
        silver: Schema.Struct({
          back_default: Schema.NullOr(Schema.String),
          back_shiny: Schema.NullOr(Schema.String),
          front_default: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String),
        }),
      }),
      "generation-iii": Schema.Struct({
        emerald: Schema.Struct({
          front_default: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String)
        }),
        "firered-leafgreen": Schema.Struct({
          back_default: Schema.NullOr(Schema.String),
          back_shiny: Schema.NullOr(Schema.String),
          front_default: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String),
        }),
        "ruby-sapphire": Schema.Struct({
          back_default: Schema.NullOr(Schema.String),
          back_shiny: Schema.NullOr(Schema.String),
          front_default: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String),
        }),
      }),
      "generation-iv": Schema.Struct({
        "diamond-pearl": Schema.Struct({
          back_default: Schema.NullOr(Schema.String),
          back_female: Schema.NullOr(Schema.String),
          back_shiny: Schema.NullOr(Schema.String),
          back_shiny_female: Schema.NullOr(Schema.String),
          front_default: Schema.NullOr(Schema.String),
          front_female: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String),
          front_shiny_female: Schema.NullOr(Schema.String),
        }),
        "heartgold-soulsilver": Schema.Struct({
          back_default: Schema.NullOr(Schema.String),
          back_female: Schema.NullOr(Schema.String),
          back_shiny: Schema.NullOr(Schema.String),
          back_shiny_female: Schema.NullOr(Schema.String),
          front_default: Schema.NullOr(Schema.String),
          front_female: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String),
          front_shiny_female: Schema.NullOr(Schema.String),
        }),
        platinum: Schema.Struct({
          back_default: Schema.NullOr(Schema.String),
          back_female: Schema.NullOr(Schema.String),
          back_shiny: Schema.NullOr(Schema.String),
          back_shiny_female: Schema.NullOr(Schema.String),
          front_default: Schema.NullOr(Schema.String),
          front_female: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String),
          front_shiny_female: Schema.NullOr(Schema.String),
        }),
      }),
      "generation-v": Schema.Struct({
        "black-white": Schema.Struct({
          animated: Schema.Struct({
            back_default: Schema.NullOr(Schema.String),
            back_female: Schema.NullOr(Schema.String),
            back_shiny: Schema.NullOr(Schema.String),
            back_shiny_female: Schema.NullOr(Schema.String),
            front_default: Schema.NullOr(Schema.String),
            front_female: Schema.NullOr(Schema.String),
            front_shiny: Schema.NullOr(Schema.String),
            front_shiny_female: Schema.NullOr(Schema.String),
          }),
          back_default: Schema.NullOr(Schema.String),
          back_female: Schema.NullOr(Schema.String),
          back_shiny: Schema.NullOr(Schema.String),
          back_shiny_female: Schema.NullOr(Schema.String),
          front_default: Schema.NullOr(Schema.String),
          front_female: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String),
          front_shiny_female: Schema.NullOr(Schema.String),
        }),
      }),
      "generation-vi": Schema.Struct({
        "omegaruby-alphasapphire": Schema.Struct({
          front_default: Schema.NullOr(Schema.String),
          front_female: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String),
          front_shiny_female: Schema.NullOr(Schema.String),
        }),
        "x-y": Schema.Struct({
          front_default: Schema.NullOr(Schema.String),
          front_female: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String),
          front_shiny_female: Schema.NullOr(Schema.String),
        }),
      }),
      "generation-vii": Schema.Struct({
        icons: Schema.Struct({
          front_default: Schema.NullOr(Schema.String),
          front_female: Schema.NullOr(Schema.String)
        }),
        "ultra-sun-ultra-moon": Schema.Struct({
          front_default: Schema.NullOr(Schema.String),
          front_female: Schema.NullOr(Schema.String),
          front_shiny: Schema.NullOr(Schema.String),
          front_shiny_female: Schema.NullOr(Schema.String),
        }),
      }),
      "generation-viii": Schema.Struct({
        icons: Schema.Struct({
          front_default: Schema.NullOr(Schema.String),
          front_female: Schema.NullOr(Schema.String)
        }),
      }),
    }),
  }),
  cries: Schema.Struct({latest: Schema.String, legacy: Schema.String}),
  stats: Schema.Array(Schema.Struct({
    base_stat: Schema.Int,
    effort: Schema.Int,
    stat: Schema.Struct({name: Schema.String, url: Schema.String}),
  })),
  types: Schema.Array(Schema.Struct({
    slot: Schema.Int,
    type: Schema.Struct({name: Schema.String, url: Schema.String}),
  })),
  past_types: Schema.Array(Schema.Struct({
    generation: Schema.Struct({name: Schema.String, url: Schema.String}),
    types: Schema.Array(Schema.Struct({
      slot: Schema.Int,
      type: Schema.Struct({name: Schema.String, url: Schema.String}),
    })),
  })),
}) {}

// ************************************************************************************
// REQUESTS
// ************************************************************************************

export class PokemonCatalogRequest extends Request.TaggedClass("PokemonCatalogRequest")<
  PokemonCatalog,
  GetPokemonCatalogError,
  {}
> {}

export class PokemonByIdRequest extends Request.TaggedClass("PokemonByIdRequest")<
  Pokemon,
  GetPokemonError,
  { readonly id: number | string }
> {}

// ************************************************************************************
// ERRORS
// ************************************************************************************

export class FetchPokemonError extends Data.TaggedError("FetchPokemonError") {}
export class DecodePokemonError extends Data.TaggedError("DecodePokemonError") {}
export class GetPokemonError extends Data.TaggedError("GetPokemonError") {}
export class GetPokemonCatalogError extends Data.TaggedError("GetPokemonCatalogError") {}

// ************************************************************************************
// TYPES
// ************************************************************************************

export type ApiRequest = PokemonCatalogRequest | PokemonByIdRequest;
export type ProgramResult = Effect.Effect<SuccessResult, ErrorResult, never>;
type SuccessResult = Pokemon | PokemonCatalog;
type ErrorResult = GetPokemonError | GetPokemonCatalogError;
