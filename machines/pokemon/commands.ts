import {Effect} from "effect";
import type {Model} from "~/machines/pokemon/model";
import {PokemonLive} from "~/machines/pokemon/repository";
import {
  GetPokemonCatalogError,
  GetPokemonError,
  Pokemon,
  PokemonByIdRequest,
  PokemonCatalog,
  PokemonCatalogRequest
} from "~/machines/pokemon/types";

// https://youtu.be/npUDDjQ9J_w?si=PUIppF3OjS4809_w&t=1356
// identity function for Effect, will let Effect infer from Error channel
// not sure how useful this will be yet considering disparate union type `ProgramResult`
const makeEffect = <A = never>() =>
  <E, R>(effect: Effect.Effect<A, E, R>) => effect;

export const getPokemonCatalog = makeEffect<PokemonCatalog>()(
  PokemonLive(
    new PokemonCatalogRequest()
  ) as Effect.Effect<PokemonCatalog, GetPokemonCatalogError>);

export const getPokemons = (ids: number[]) =>
  makeEffect<Pokemon[]>()(
    Effect.forEach(
      ids,
      (id) => PokemonLive(
        new PokemonByIdRequest({ id })
      ) as Effect.Effect<Pokemon, GetPokemonError>
    )
  );

export const onInit = (ids: number[]) =>
  makeEffect<Partial<Model>>()(
    Effect.all({
      catalog: getPokemonCatalog,
      table: getPokemons(ids),
    })
  );
