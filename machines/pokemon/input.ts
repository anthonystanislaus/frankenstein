export interface Startup {
  source: number;
}

export interface Page {
  page: number;
}

export type Input = Startup | Page;
