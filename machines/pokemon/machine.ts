import {Array, Effect} from "effect";
import {assign, fromPromise, setup} from "xstate";
import {getPokemonCatalog, getPokemons, onInit} from "~/machines/pokemon/commands";
import {type Msg} from "~/machines/pokemon/events";
import type {Input, Page} from "~/machines/pokemon/input";
import {initialModel, type Model} from "~/machines/pokemon/model";
import type {Pokemon, PokemonCatalog} from "~/machines/pokemon/types";

export const pokemonMachine = setup({
  types: {
    context: {} as Model,
    events: {} as Msg,
    input: {} as Input,
  },
  actors: {
    onInit: fromPromise<Partial<Model>, Page>(
      ({ input: { page } }) =>
        Effect.runPromise(onInit(Array.range(page, page * 15)))
    ),
    getPokemonCatalog: fromPromise<PokemonCatalog>(
      () => Effect.runPromise(getPokemonCatalog)
    ),
    getPokemons: fromPromise<Pokemon[], Page>(
      ({ input: { page } }) =>
        Effect.runPromise(getPokemons(Array.range(page, page * 15)))
    ),
  },
  actions: {
    logger: ({ event }) => console.log("logger", event),
  },
  guards: {
    pageClamp: () => false,
  },
  delays: {},
}).createMachine({
  id: "@app.machine/pokemon",
  context: initialModel,
  initial: "ready",
  invoke: {
    src: "onInit",
    input: ({ context: { page } }) => ({ page }),
    onDone: {
      target: ".ready",
      actions: assign(({ event }) => ({ ...event.output })),
    },
    onError: { target: ".error" },
  },
  states: {
    ready: {
      on: {
        "page.prev": {
          actions: { type: "logger" },
        },
        "page.next": {
          actions: { type: "logger" }
        },
      }
    },
    error: {},
  },
});
