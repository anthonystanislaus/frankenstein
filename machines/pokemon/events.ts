import type {Input, Page} from "~/machines/pokemon/input";

/** https://stately.ai/docs/input#initial-event-input */
export interface AutoInit {
  readonly type: "xstate.init";
  readonly input: Input;
}

export interface PageTurn extends Page {
  readonly type: "page.prev" | "page.next";
}

export type Msg = AutoInit | PageTurn;
