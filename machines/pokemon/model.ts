import {Schema} from "@effect/schema";
import {Pokemon, PokemonCatalog} from "~/machines/pokemon/types";

export class Model extends Schema.Class<Model>("Model.Pokemon")({
  catalog: Schema.NullOr(PokemonCatalog),
  table: Schema.Array(Pokemon),
  page: Schema.Positive,
}) {}

export const initialModel = new Model({
  catalog: null,
  table: [],
  page: 1,
});
