import {Effect, Number} from "effect";
import * as Model from "~/machines/toggle/model";

export const resetCount = (
  { count }: Model.Model
): Effect.Effect<Partial<Model.Model>> =>
  Effect.gen(function* (_) {
    return yield* _(Effect.sync(() => ({ count: Number.multiply(count, 0) })));
  });

export const incrementCount = (
  { count }: Model.Model,
): Effect.Effect<Partial<Model.Model>> =>
  Effect.gen(function* (_) {
    return yield* _(Effect.sync(() => ({ count: Number.increment(count) })));
  });
