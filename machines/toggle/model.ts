import * as Schema from "@effect/schema/Schema";

export class Model extends Schema.Class<Model>("Model.Toggle")({
  count: Schema.Int,
  maximum: Schema.Positive,
}) {}

export const initialModel = new Model({
  count: 0,
  maximum: 11,
});
