import * as E from "effect/Effect";
import {assign, setup} from "xstate";
import * as Actions from "~/machines/toggle/actions";
import * as Msg from "~/machines/toggle/events";
import * as Input from "~/machines/toggle/input";
import * as Model from "~/machines/toggle/model";

export const toggleMachine = setup({
  types: {
    input: {} as Input.Input,
    context: {} as Model.Model,
    events: {} as Msg.Msg,
  },
  actors: {},
  actions: {
    resetCount: assign(({ context }) => E.runSync(Actions.resetCount(context))),
    incrementCount: assign(({ context }) => E.runSync(Actions.incrementCount(context))),
  },
  guards: {
    isLive: ({ context: { count, maximum } }) => count < maximum,
  },
  delays: { wait500ms: 500, wait2000ms: 2000, wait5000ms: 5000 },
}).createMachine({
  id: "@app.machine/toggle",
  context: Model.initialModel,
  initial: "inactive",
  states: {
    inactive: {
      on: {
        TOGGLE: [
          { target: "active", guard: "isLive" },
          { target: "hibernate" },
        ]
      }
    },
    active: {
      entry: "incrementCount",
      after: { wait500ms: "inactive" }
    },
    hibernate: {
      after: { wait5000ms: "inactive" },
      exit: "resetCount",
    }
  }
});
