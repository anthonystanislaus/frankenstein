import * as Input from "~/machines/toggle/input";

/** https://stately.ai/docs/input#initial-event-input */
export interface AutoInit {
  readonly type: "xstate.init";
  readonly input: Input.Input
}

export interface Toggle {
  readonly type: "TOGGLE";
}

export type Msg = AutoInit | Toggle;
